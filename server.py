import BaseHTTPServer
import os

class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    HTDOCS = "/home/osirv/pi_labs/loncaric/se_lab6/htdocs"

    def do_GET(self):
        page = self.handle_file(self.path)
        if page:
            self.send_content(200, page)
        else:
            self.handle_error(404, "File not found.")

    def handle_file (self, file_path):
        full_path = self.HTDOCS + file_path
        print "File: ", full_path
        if os.path.isfile(full_path):
            with open(full_path, "rb") as reader:
                content = reader.read()
            return content
        else:
            return none

    def handle_error(self, status_code, message):
    page += "<html><body><p>"
    page += message
    page += "</p></body></html>"
    self.send content (status_code, message)

    def send_content(self, status_code, content):
        self.send_response(status_code)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Lenght", str(len(content)))
        self.end_headers()
        self.wfile.write(content)
        

if __name__ == "__main__":
    print "Starting web server..."
    server_address = ('', 8080)
    server = BaseHTTPServer.HTTPServer(server_address, RequestHandler)
    server.serve_forever()

